---
title: September 2020 virtual meetup
date: 2020-09-01 15:00:00 +0530
desc: September 2020 virtual meetup
category: event
---

The September 2020 meeting of <em>Indian Linux User Group, Mumbai (ILUG-BOM)</em>,
will be held virtually on 26th September 2020, 5:00 PM onwards.

**Venue**:

Virtual Meeting (Link will be shared closer to the event)

**Agenda:**

<ol>
	<ul>Open Source: Legal Strategy - Avoiding Pitfalls By Biju K. Nair</ul>
    <ul>Containers, introduction and some foundational concepts By Sohom Bhattacharjee</ul>
</ol>