---
title: August 2017 meetup
date: 2017-08-05 09:16:01 +0530
desc: August 2017 meetup at Don Bosco Institute of Technology.
category: event
---

The August 2017 meeting of "Indian Linux User Group, Mumbai (ILUG-BOM)",
was held on 12th August 2017, 15:00 to 16:00 IST.

**Venue**:  

IT Lab 7, First Floor, D - Wing,  
Department of Information Technology,  
Don Bosco Institute of Technology, Kurla (W).  
10 min from Vidyavihar Station (W).  
<a href="https://goo.gl/maps/fRQoetu97Vp" target="_blank">View Map</a>

**Agenda:** 
1. 30 mins. - Basics of [GIMP](https://www.gimp.org/) & Image editing - Raghavendra Kamath.
2. Floor open for relevant discussions.

**Meeting Summary**

<div align="justify">
The meeting started with Gimp talk by Raghavendra Kamath. Everyone wants to play with shapes and colors right from childhood days. Apart from that, basic image processing is everyday need of Digital Age.

We often need to work with Image formats and their conversion. Uploading identity card size  photos in applications, résumé etc. Without appropriate tools and proper guidance this can be too difficult or even impossible for average user. Fortunately we had more student attendees who certainly enjoyed the expert's tips. 

It is true that anyone can practice and teach elementary gimp usage. But learning basics from a professional artist is really a different experience. We felt this difference when Raghu started experimenting with sample images. He showed how to colorize original black and white image with various color tools.  He finally gave a demonstration of masking where he changed the face of a person in image, keeping the same body. The transformation was so homogeneous that it was impossible to suspect any manipulation. Of course he warned the students not to misuse this skill.

In the next session, Joe put a proposal of celebrating Software Freedom Day (16 September). He suggested to arrange an install festival, a hackathon and FOSS exhibition in DBIT. The idea was to invite schools and colleges of Mumbai students and teachers. 

Although the idea was welcome by DBIT Profs and students, it could be difficult to arrange everything in a short amount of time.
So we put the proposal to DBIT for further discussion. 

In the last session, Nilesh showed the lab manuals that he has prepared with the help of students. Nilesh and his students team have made a wonderful job. The students had even gone to few schools. But the principals of junior colleges are not yet confident to migrate. As per his teams feedback, the only problem they faced was Tally and .net.  We need to study alternatives to these softwares further.
As far as schools are concerned there seems no problem in State or CBSE board.
</div>

[View original post on the mailing list](http://mm.ilug-bom.org.in/pipermail/linuxers/Week-of-Mon-20170731/075056.html).  
[Meeting Summary on mailing list](http://mm.ilug-bom.org.in/pipermail/linuxers/Week-of-Mon-20170814/075060.html)
