---
layout: page
---

<div class="lead lead-about pretty-links">
Upcoming event: [September 2020 Virtual Meetup](/2020/september-2020-meetup).
</div>

### Welcome to ILUG-BOM!
The GNU/Linux Users Group of Mumbai, India, also called ILUG-BOM, is an
informal mailing list of over 1200 members from all over India, but
mostly from Mumbai.

We discuss issues related to promoting, supporting the GNU/Linux OS,
various distributions, sharing free/open source resources, have
informal and formal meetings, seminars and organise workshops now and then. [More &rarr;](/events)

We also help schools and institutions in migrating to free/libre and opensource based solutioins. To contact us for migration related queries please drop us a mail at *ilug.mumbai@gmail.com*
